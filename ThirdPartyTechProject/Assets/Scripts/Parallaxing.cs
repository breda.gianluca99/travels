using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Parallaxing : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] Renderer[] backgrounds;
    [SerializeField] float[] currentBackgroundSpeed;
    
    private void Update()
    {
        Parallax();
    }

    private void Parallax()
    {
        for (int i = 0; i < backgrounds.Length; i++)
        {
            backgrounds[i].material.mainTextureOffset += new Vector2(currentBackgroundSpeed[i] * Time.deltaTime, 0);
        }
    }
    public void EndPrlx()
    {
        for (int i = 0; i < backgrounds.Length; i++)
        {
            currentBackgroundSpeed[i] = 0f;
        }
    }
}