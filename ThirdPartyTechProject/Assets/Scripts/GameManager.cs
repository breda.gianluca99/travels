using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public Vector3 endPos;
    public Parallaxing parallax;
    public GameObject StartingVirtualCam;
    public GameObject GameVirtualCam;
    public GameObject FinalVirtualCam;
    public RectTransform title;
    public SpeechBubble sb;
    public MoveTweenToTarget mttt;

    public void StartGame()
    {
        title.GetComponent<CanvasGroup>()
            .DOFade(0f, 1f)
            .OnComplete(DeactivateTitle);
        StartCoroutine(CoStartGame(2f));
        StartingVirtualCam.SetActive(false);
        GameVirtualCam.SetActive(true);
    }
    private void DeactivateTitle()
    {
        title.gameObject.SetActive(false);
    }

    public void EndGame()
    {
        GameVirtualCam.SetActive(false);
        FinalVirtualCam.SetActive(true);
        StartCoroutine(CoEndGame(0.25f));
    }
    IEnumerator CoStartGame(float time)
    {
        yield return new WaitForSeconds(time);
        sb.can = true;
    }
    IEnumerator CoEndGame(float time)
    {
        yield return new WaitForSeconds(time + 0.5f);
        mttt.MoveToTarget();
        yield return new WaitForSeconds(time);
        parallax.EndPrlx();
        //FinalVirtualCam.SetActive(true);
    }
}