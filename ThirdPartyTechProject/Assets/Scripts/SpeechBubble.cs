using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Yarn.Unity;
public class SpeechBubble : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private RectTransform speechBubbleTranform;
    [SerializeField] private RectTransform playerBubbleTranformPos;
    [SerializeField] private DialogueRunner _playerSpeech;
    [SerializeField] private string _startingNode = "Start";
    bool interacted = false;
    public bool can = false;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && !interacted && can)
        {
            Speak();
            interacted = true;
        }
    }

    public void Speak()
    {
        Sequence mySequence = DOTween.Sequence();
        RectTransform t = _canvasGroup.gameObject.GetComponent<RectTransform>();
        mySequence.Insert(0f, _canvasGroup.DOFade(1f, 1f))
            .Insert(0f, speechBubbleTranform.DOAnchorPos(playerBubbleTranformPos.anchoredPosition, 1f, false))
            .Insert(0f, speechBubbleTranform.DOScale(1f, 1f))
            .OnComplete(StartSpeak);
    }

    public void StartSpeak()
    {
        _playerSpeech.NodeExists(_startingNode);
        _playerSpeech.StartDialogue(_startingNode);
    }
}