using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class MoveTweenToTarget : MonoBehaviour
{
    public Vector3 endPos = Vector3.zero;
    public float duration = 0f;

    public void MoveToTarget()
    {
        transform.DOMove(endPos, duration);
    }
}
